# LVST

The code in "lvst.sol" is a smart-contract for Ethereum platform.

The contract was not deployed yet.


## What is LVST

LVST is a custom token based on Ethereum smart-contract. LVST tokens is direct conversionable to ether at any time. You can buy new tokens with ether, send LVST directly and convert your tokens to ether. There are no any fees except a gas cost. 

### LVST is not a pyramid scheme

There is no profit for early adopters. 
